package Bronze_Chapter4;

public class Q18 {
	public static void main(String[] args) {
		for (int i = 0; i < 2; i++) {

			// j += i の式の意味は j = j + i　になり、j = 0+0ずっと増えないので、無限ループ
			for (int j = 0; j < 2; j += i) {
				System.out.println("LOOP");
			}
		}
	}
}
