package Bronze_Chapter4;

public class Sample_do_while {

	public static void main(String args[]) {
		int i = 7;

		//while文とdo-while文の違いは、条件式が初めからfalseの場合に
		//while文ではブロック内の処理が1度も行われないのに対して、
		//do-while文ではブロック内の処理が1度は行われる点です。

//		do{
//			　実行する処理
//			} while(条件式)

		do {
			System.out.println("i = " + i);
			i -= 4;
		} while (i > 0);

		System.out.println("0を下回ったので終了です");

		i = -3;

		do {
			System.out.println("i = " + i);
			i -= 4;
		} while (i > 0);

		System.out.println("0を下回ったので終了です");

		System.out.println("------------------------\n---------------------------------\n----------");

		int i2 = 0;
		do {
			System.out.println((i2 + 1) + "回目の処理です");
			i2++;
		} while (i2 < 3);
		System.out.println("処理が終了しました");

	}
}
