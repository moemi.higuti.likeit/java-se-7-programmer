package Bronze_Chapter4;

public class Q12 {
	public static void main(String[] args) {
		int[] array = { 0, 2, 4, 6 };
		for (int e : array) {
			System.out.println(e + " ");
		}
	}
}

//for文
//for (int i = 0; i <array.length; i++) {
//	System.out.println(array[i]);
//}

//拡張for文
//for(int e : array) {
//	System.out.println(e);
//}