package Bronze_Chapter4;

public class Q5 {

	public static void main(String[] args) {

		for (int i = 0;//初回のみで実行される
				i<5; //boolean型に戻す。for文を実行されるのかどうか確かめる
				i++ //変数をインクリメントして、上のbooleanの式で確かめる
				) {
			System.out.println("LOOP");
		}
	}
}

//for(初期化式;　条件式　;　反復式) {
//
//}

//
//int i = 0; i++; i<5

//iを定義していないのでエラーが起こる
//i<5; int i = 0; i++

//iを定義していないのでエラーが起こる
//i = 0; i<5;i++

//これが正解
//int i = 0; i<5; i++