public class P558list14_2 {

	String accountNumber;
	int balance;


	P558list14_2(String accountNumber, int balance) {
	super();
	this.accountNumber = accountNumber;
	this.balance = balance;
}

	public String toString() {
		return "\\" + this.balance + "(口座番号　：　" + this.accountNumber + ")";
	}

	public boolean equals(Object o) {
		if (this == o) {
			return true;
		}

		if (o instanceof P558list14_2) {
			P558list14_2 a = (P558list14_2) o;
			String an1 = this.accountNumber.trim();
			String an2 = a.accountNumber.trim();

			if (an1.equals(an2)) {
				return true;
			}
		}

		return false;
	}

}
