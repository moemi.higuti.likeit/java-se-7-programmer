import java.util.Calendar;
import java.util.Date;

public class P537_14_3 {

	public static void main(String[] args) {

		//現在の年を表示する
		Date now = new Date();
		Calendar c = Calendar.getInstance();
		c.setTime(now);

		int y = c.get(Calendar.YEAR);

		System.out.println("今年は" + y + "年です");

		//指定した斌夫Date型のあたりを得る
		c.set(2020, 8, 25, 1, 24, 20);

		//1853年　黒船来航
		//ここでカレンダーインスタンス　c の年だけを変える
		c.set(Calendar.YEAR, 1853);

		//月を指定したい場合は、1~12ではなく　0~11　0月が1月
		c.set(Calendar.MONTH, 0);

		c.set(Calendar.DAY_OF_MONTH, 1);

		c.set(Calendar.HOUR, 1);
		c.set(Calendar.MINUTE, 1);
		c.set(Calendar.SECOND, 1);

		Date past = c.getTime();
		System.out.println(past);
	}

}
