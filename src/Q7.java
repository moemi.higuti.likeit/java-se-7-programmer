public class Q7 {

	public static void main(String[] args) {
		try(Sample sample = new Sample()){

			//自分の答え
//			System.out.println("B");

			//①　まず　クラスSampleのCloseメソッドが実行される
//			RuntimeException
//			実行時に起こった例外で必ずしも対処しなくても良い事象。例外処理を書くか書かないかは状況によります。
			//③例外処理を起こさせる
			throw new RuntimeException("A");
		}
		catch (Exception e) {
			//④　上から強制的に起こした例外引数Aを受け取って実行される（A）
			System.out.println(e.getMessage());
		}finally {
			//⑤　最後にファイナリーが実行される
			System.out.println("C");
		}
	}

}
