import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public class P558list14_1 {

	public static void main(String[] args) {

		// ①　現在の日時をデータ型で取得
		Date now = new Date();

		//②　取得した日時情報をCalendarにセット
		Calendar c = Calendar.getInstance();

		// まずは、現在日時を取得
		c.setTime(now);

		//先ほどセットした現在日時で、カレンダーで取得する
		int day = c.get(Calendar.DAY_OF_MONTH);

		//カレンダーとして、取得した日時　からそこから100日プラスする
		day += 100;

		//上で足したものをセットする
		c.set(Calendar.DAY_OF_MONTH, day);

		//それを新しい変数に入れる
		Date future = c.getTime();

		//フォーマットクラスを呼び出し、型を決める
		SimpleDateFormat f = new SimpleDateFormat("西暦yyyy年MM月dd日");

		//それを出す
		System.out.println("今日は　\n"+f.format(now));

		System.out.println("--------------------------------------");

		System.out.println("今日から100日後は　\n"+f.format(future));

	}
}
