import java.util.Scanner;

public class Main {

	public static void main(String[] args) {

		//class P558list14_2 をもとにインスタンス暗証番号と残高を引数にして、インスタンスを作る
		P558list14_2 a = new P558list14_2("4649", 1592);

		//System.out.println(a);
		while (true) {
			//文字列の表示
			System.out.println("暗証番号を入れてください");

			// 入力を要求するためのもの
			Scanner scanner = new Scanner(System.in);

			//String型で入力されたものを受け取る
			String pin;
			pin = scanner.nextLine();

			//もしも、pin と上で作られた暗証番号が同じならなど条件分岐する
			if (a.accountNumber.equals(pin)) {

				System.out.println(a);

			} else {
				System.out.println("暗証番号が違います");

			}
		}
	}
}
