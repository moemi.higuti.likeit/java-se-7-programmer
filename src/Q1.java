
public class Q1 {

	//この問題の趣旨はどことどれを比べているのかの問題

	//フィールド　クラス直下で定義された変数
	private final static String A = "a";
	private final static String B = "b";

	//メインメッソド
	public static void main(String[] arg) {

		//String型の変数にprivate final static String B

		//もしインスタンスを作り”b”をもとに変数名　Bと、下のケースで比べられる
		String str = new String("b");

		//　もしも下記の形で、比較した場合、変数の中身と下のstrの中身を比べられる
		//String str ="b";

		//もしも下記の形なら、変数の中身　小文字のabと大文字を比べられるので、デフォルトが反映される
		//String str ="B";

		switch (str) {
		case A:
			System.out.println("A");
			break;

		case B:
			System.out.println("B");
			break;

		default:
			System.out.println("D");
			break;

		}
	}
}
