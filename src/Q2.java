
public class Q2 {

	public static void main(String[] arg) {

		//フィールド　クラス直下の変数
		//		String obj = "A";
		//		String obj = "B";
		String obj = null;

		//問題ではSwitch文だったが、if 文のほうが効率が上がる可読性も上がる
		//設問のような場合は　じぜんにnull チェックをするか、If文を作る

//ーーーーーーーー自分で作る switc　文の前にnull のチェック　あらかじめデフォルトと同じ処理で作るか　違う値を入れるかーーーーーーーーーーーーーーーーーーーー

		if (obj == null) {
			System.out.println("default");
			//obj = "C"


		} else {

//ーーーーーーーー自分で作るーーーーーーーーーーーーーーーーーーーー

			switch (obj) {

			case "A":
				System.out.println("A");
				break;

			case "B":
				System.out.println("B");
				break;

			default:
				System.out.println("default");
				break;
			}

		}

//ーーーーーーーー自分で作る()ーーーswitch文ではなく代わりにIf文でーーーーーーーーーーーーーーーーー
//		if(obj =="A") {
//			System.out.println("A");
//		}else if(obj=="B") {
//			System.out.println("B");
//		}else {
//			System.out.println("default");
//		}

	}

}
