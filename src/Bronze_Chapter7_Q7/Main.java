package Bronze_Chapter7_Q7;

public class Main {

	public static void main(String[] args) {

		//引数なしのインスタンスを作る　親クラスSuperClassコンストラクタ実行　Aが出力。　子クラスのSubClassが実行される
		new SubClass();


		System.out.println("");

		//引数ありのインスタンスを作る。親クラスSuperClassコンストラクタ実行　Aが出力。　
		//子クラスのSubClassの引数ありのコンストラクタがそのまま実行される
		new SubClass("('ω')ノ");

		System.out.println("");

		new SubClass("B");



	}

}
