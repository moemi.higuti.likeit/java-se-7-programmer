package Bronze_Chapter7_Q7;

public class SubClass extends SuperClass {

	public SubClass() {

		//同じクラスの引数ありのコンストラクタが呼び出しされて、実行
		//this("B");//　答え

		//再帰呼び出し、再帰呼出 メソッドが、自分自身のメソッドを呼び出すこと。
		//this();

		//親クラスのコンストラクタの呼び出しは一回だけ
		//親クラスの引数なしのコンストラクタの呼び出し
		//super();

		//親クラスの引数ありのコンストラクタの呼び出し変数valに入れられ、Bだけ出力される
		//super("B");

		//メソッド扱い
		//SuperClass("B");
		//SubClass("B");

		//コンストラクタの呼び出しは一回のみ
//		this("B");
//		super();
//		this("B");

		// *① 下の引数ありのコンストラクタとセット
		//this("B");

	}
	public SubClass(String val) {

		// *①　とセットになる
		super();

		System.out.println(val);
	}
}
