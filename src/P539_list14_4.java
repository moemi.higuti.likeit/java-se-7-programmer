import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class P539_list14_4 {

	public static void main(String[] args) throws ParseException {

		//Date 型のインスタンスを作る
		Date now = new Date();

		//フォーマットを変えずに出力
		System.out.println("これがDate型さ\n" + now);

		//SimpleDateFormatクラスを利用するため呼び出す
		SimpleDateFormat f = new SimpleDateFormat("yyyy/mm/dd hh:mm:ss");

		//DateからStringを生成する
		String s = f.format(now);

		System.out.println("-------------------------------------------");

		System.out.println("これが人間用\n" + s);

		//指定日時の文字列を解析しDate型として得る

		System.out.println("-------------------------------------------");

		//try chachがここで必要になる
		//()の中に入れたString型をDateを生成する
		Date d = f.parse("2011/09/22 01:23:45");
		System.out.println("Stringa型からDate型に変えたよ\n" + d);
	}
}
