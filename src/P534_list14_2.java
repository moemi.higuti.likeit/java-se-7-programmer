//インポートしておくと便利
import java.util.Date;
public class P534_list14_2 {

	//メインメソッドを作る
	public static void main(String[] args) {

		//現在日時を持つ Date インスタンスの生成
		Date now = new Date();

		//上で作ったインスタンスを表示
		System.out.println(now);

		//long値で取り出したい場合　”getTime()”メソッドを使う
		System.out.println(now.getTime());

		//long値の日時を持つインスタンスを生成
		Date past = new Date(1316622225935L);

		//上でセットされた日時を出す
		System.out.println(past);

	}

}
