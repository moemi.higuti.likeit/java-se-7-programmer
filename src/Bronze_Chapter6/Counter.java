package Bronze_Chapter6;

public class Counter {

	//初期値は、0
	static int count = 0;

	//int 型で、count　が返される
	public static int getCount() {
		return count;
	}

	//実行されたらcountが増える
	public static void incrementCount() {
		count++;
	}
}
