package Bronze_Chapter6;

public class Sample2 {

	private int a;
	private int b;

	public Sample2() {

		this(10);
		b = 5;

		//コンストラクタ内でオーバーロードをしたい場合は、コンストラクタブロック内の先頭行以外ですると
		//コンパイラエラーになる
		//this(10);
	}

	public Sample2(int a) {
		this.a = a;
		this.b = -5;
	}

	public void hello() {
		System.out.println(a+" , "+b);
	}

}
