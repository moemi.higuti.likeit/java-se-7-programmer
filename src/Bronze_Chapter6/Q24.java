package Bronze_Chapter6;

public class Q24 {

	public static void main(String[] args) {
		Counter c1 = new Counter();
		Counter c2 = new Counter();
		Counter c3 = new Counter();

		//incrementCount()が０回実行されたとき
		System.out.println("-----incrementCount()が０回実行されたとき---------");
		System.out.println(Counter.getCount());
		System.out.println("--------------");

		// Counter クラスの　incrementCount()を呼び出すたびにメソッドが実行される　→カウント数が増える
		c1.incrementCount();

		Counter.incrementCount();

		//incrementCount()が一回実行されたとき
		System.out.println("---incrementCount()が一回実行されたとき-----------");
		System.out.println(Counter.getCount());
		System.out.println("--------------");
		c2.incrementCount();

		System.out.println(c1.getCount());
		System.out.println(c3.getCount());
		System.out.println(Counter.getCount());
	}
}
