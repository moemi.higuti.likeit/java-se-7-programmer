package Bronze_Chapter6;

public class Book {

	//フィールド　変数の集合体
	private String title;
	private int price;

	//引数なしのコンストラクタ
	public Book() {
		this("none",0);

	}
	//上の引数なしのコンストラクタから、オーバーライドされた引数ありの
	//コンストラクタを呼び出す

	public Book(String title, int price) {
		this.title = title;
		this.price = price;
	}
	//メソッド
	public void print() {
		System.out.println(title + " , " + price);
		System.out.println("今日もいい天気");
	}
}
