package Bronze_Chapter6;

public class ClassQ23 {

	//staticのメソッドでインスタンスのフィールドは使えない
	//	private int number = 0;
	private static int number = 0;

	public static int getNumber() {

		return number;
	}
}
