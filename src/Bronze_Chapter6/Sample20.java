package Bronze_Chapter6;

public class Sample20 {

	public static void main(String[] args) {
		Student s1 = new Student();
		Student s2 = new Student("ひと２");
		Student s3 = new Student("人３");
		s1.name = "name";
		s1.info();
		s2.info();
		s3.info();
	}

}
