package asobi;

public abstract class Character {

	//フィールド
	private String name;
	private String attribute;
	private int attack;
	private int hp;

	//コンストラクタ
	public Character(String name, String attribute, int attack, int hp) {
		super();
		this.name = name;
		this.attribute = attribute;
		this.attack = attack;
		this.hp = hp;
	}

	@Override
	public String toString() {
		return "パラメーターだよ [名前=" + name + ", 個性=" + attribute + ", attack=" + attack + ", hp=" + hp + "]";
	}

	//getter
	public abstract String greeting();


}
