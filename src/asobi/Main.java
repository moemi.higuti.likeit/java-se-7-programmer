package asobi;

import java.util.Random;
import java.util.Scanner;

public class Main {

	public static void main(String[] args) {

		// 入力を要求するためのもの
		Scanner scanner = new Scanner(System.in);
		// 乱数生成処理
		Random random = new Random();

		while (true) {
			System.out.println("---------------------------------------------\n");
			System.out.println("キャラクターを作ります");
			System.out.println("");
			System.out.println("0:こうげきたろう,   1:体力次郎,   2:普通の人");
			System.out.println("\n---------------------------------------------");
			int player = Integer.parseInt(scanner.nextLine());
			System.out.println("---------------------------------------------\n");
			System.out.println("何か一言どうぞ");
			System.out.println("\n---------------------------------------------");
			String word = scanner.nextLine();

			if (player == 0) {

				Cara cara = new Cara("こうげきたろう", "アッタク", 100, 10);
				System.out.println(cara.toString());
				System.out.println(cara.oneword(word));

			} else if (player == 1) {
				Cara cara = new Cara("体力次郎", "体力", 10, 100);
				System.out.println(cara.toString());

			} else if (player == 2) {
				Cara cara = new Cara("普通の人", "ふつう", 55, 55);
				System.out.println(cara.toString());

			}
		}
	}

}
