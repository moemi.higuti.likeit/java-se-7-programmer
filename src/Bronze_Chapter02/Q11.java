package Bronze_Chapter02;

public class Q11 {
	public static void main(String[] args) {
		final String COLOR = "blue";
		//other color
		//修飾子にfinalがあるので変数の中身を変えるのは無理

		//COLOR = "red";
		System.out.println(COLOR);
	}
}
