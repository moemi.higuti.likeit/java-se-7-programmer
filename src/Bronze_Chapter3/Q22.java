package Bronze_Chapter3;

public class Q22 {
	public static void main(String[] args) {
		int a = 1;
		switch (a) {

		case 1:
			System.out.println("A");
			//breakがないので通り抜けて、case 2にいく

		case 2:
			System.out.println("B");
			//breakがないので通り抜けて、case 3にいく

		case 3:
			System.out.println("C");
		}
	}
}
