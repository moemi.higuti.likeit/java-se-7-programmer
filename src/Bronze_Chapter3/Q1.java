package Bronze_Chapter3;

public class Q1 {

	public static void main(String[] args) {

		//整数同士の割り算なので、２が表示される
		System.out.println(5 /2);

		//どれかに小数点を入れると、2.5の表示がされる
		System.out.println(5.0 /2);
		System.out.println(5 /2.0);
	}
}
