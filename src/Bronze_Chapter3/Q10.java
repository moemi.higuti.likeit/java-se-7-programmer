package Bronze_Chapter3;

public class Q10 {

	public static void main(String[] args) {

		int a = 2;
		int b = 5;

		int c = 1;
		int d = 1;

		//インクリメント演算子　デクリメント演算子

		//前置で++　が置かれているんで、実行される前に、a に１プラスされる
		//後置で--　が置かれている場合は、実行される後に　bい１マイナスされる
		System.out.println(++a +b--);
		System.out.println("実行されたからbの値がかわったよ　　"+b);

		System.out.println("");


		System.out.println(++c);
		System.out.println(c);

		System.out.println(d--);
		System.out.println(d);

	}
}
