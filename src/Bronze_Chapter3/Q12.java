package Bronze_Chapter3;

public class Q12 {

	public static void main(String[] args) {

		//if文の{}は省略できる
		if (false)
			System.out.println("A");
		System.out.println("B");
		System.out.println("C");

		System.out.println("");

		if (true) {
			System.out.println("1");
			System.out.println("2");
			System.out.println("3");
		}

		System.out.println("");
		//条件式の中をTrueにする
		if (true)
			System.out.println("a");
		System.out.println("b");
		System.out.println("c");

		//		//if文の{}は省略できる
		//		if (false) {
		//falseの状態ではないのでこの文は飛ばされる
		//			System.out.println("A");
		//		}
		//		System.out.println("B");
		//		System.out.println("C");

		int i = 0;

		if (i == 0) {
			System.out.println(i);
			System.out.println(i);
			System.out.println(i);
		}

		//if文として働くのは、次の一文のみ
		if (i == 0) System.out.println(i);

			System.out.println(i);
			System.out.println(i);
	}
}
