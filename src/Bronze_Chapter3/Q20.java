package Bronze_Chapter3;

public class Q20 {

	public static void main(String[] args) {
		int a = 1;
		switch (a) {

		case 1:
			System.out.println("A");
			//breakがないので通り抜けて、case 2にいく

		case 2:
			System.out.println("B");
			break;

		case 3:
			System.out.println("C");
			break;
		}
	}
}
