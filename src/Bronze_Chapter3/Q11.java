package Bronze_Chapter3;

public class Q11 {

	public static void main(String[] args) {

		int a = 0;

		//条件式の結果は、boolean型の値が必要
		//下は、ただ変数に代入しているだけ
		//			if(a = 0)
		//				System.out.println("A");

		if (a == 0)
			System.out.println("A");
		if (a != 0)
			System.out.println("B");
	}
}