package textbook;

public abstract class Assent {

	private String name;
	private int price;

	public Assent(String name, int price) {
		super();
		this.name = name;
		this.price = price;
	}

	public String getName() {
		return name;
	}

	public int getPrice() {
		return price;
	}

}
