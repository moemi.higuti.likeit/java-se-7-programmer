package textbook;

public interface Thing {

	public double getWeight();
	public void setWeight(double weight);

}
