package textbook;

public class P491_Computer extends TrangibleAsset {

	//フィールド
	//	private String name;
	//	private int price;
	//	private String color;
	private String makerName;

	//	サブクラスではスーパークラスのコンストラクタを「super」句を使って継承する必要があります。
	//	サブクラスはインスタンス化できるので、サブクラスをインスタンス化してスーパークラスから継承した
	//	コンストラクタを使用します。サンプルコードで確認しましょう。

	//コンストラクタ
	//	public P491_Computer(String name, int price, String color, String makerName) {
	//		this.name = name;
	//		this.price = price;
	//		this.color = color;
	//		this.makerName = makerName;
	//	}

	public P491_Computer(String name, int price, String color, String makerName) {
		super(name, price, color);
		this.makerName = makerName;
	}

	//	//getter メソッド
	//	public String geGtName() {
	//		return name;
	//	}
	//
	//	public int getPrice() {
	//		return price;
	//	}
	//
	//	public String getColor() {
	//		return color;
	//	}

	public String getMakerName() {
		return makerName;
	}
	public String  getColor(){
		System.out.println("makerName");
		return makerName;
	}

}
