package textbook;

public abstract class TrangibleAsset extends Assent implements Thing {

	//共通の項目を探して、フィールドを設定する
	//	private String name;
	//	private int price;
	private String color;
	private double weight;

	//コンストラクタ
	//インスタントを作る
	public TrangibleAsset(String name, int price, String color) {

		//親クラスを継承することになったので、
		//		this.name = name;
		//		this.price = price;

		super(name, price);
		this.color = color;
	}

	//getter
	//	public String getName() {
	//		return name;
	//	}
	//
	//	public int getPrice() {
	//		return price;
	//	}

	public abstract String getColor() ;

	public double getWeight() {
		return weight;
	}

	public void setWeight(double weight) {
		this.weight = weight;
	}

}
