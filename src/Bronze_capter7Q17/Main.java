package Bronze_capter7Q17;

public class Main {
	public static void main(String[] args) {
		//クラスB　のインスタンスを作る
		B b = new B();
		C c = new C();

		//A型の変数 a を bにする
		A a = b;
		C ac = (C) a;

		//a を　C型にキャストする
		A aa = c;
		b = (B) a;
		c = (C) b;

	}
}
